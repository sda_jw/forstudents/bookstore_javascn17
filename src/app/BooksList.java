package app;

import java.util.ArrayList;

// klasa BooksList
public class BooksList {
    // posiada pole, będące listą dostępnych książek
    public ArrayList<Book> books = new ArrayList<Book>();

    // metoda dodająca książkę do listy
    public void addBook(Book book) {
        books.add(book);
    }
}
