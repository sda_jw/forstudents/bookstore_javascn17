package app;

// klasa Book
public class Book {
    // posiada trzy pola
    String title;
    String author;
    double price;

    // konstruktor sparametryzowany klasy Book
    // tworzy nam książkę o określonym tytule, autorze i cenie
    public Book(String t, String author, double p){
        title = t;
        this.author = author;
        price = p;
    }
}
