package app;

import java.util.ArrayList;

// klasa Basket
public class Basket {
    // posiada pole, będące listą książek w koszyku
    public ArrayList<Book> shoppingList = new ArrayList<Book>();

    // metoda zwracająca listę książek w koszyku
    public ArrayList<Book> returnBasket() {
        return shoppingList;
    }
}
