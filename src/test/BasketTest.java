package test;

import app.Basket;
import app.Book;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.assertj.core.api.Assertions.*;

// klasa testowa, testuje klasę Basket
public class BasketTest {
    // posiada pole typu Basket
    // będziemy przypisywać do niego nowe koszyki i na nich testować
    Basket myBasket;

    // metoda cyklu życia, wykonuje się przed każdym testem
    // tworzymy w niej nowy koszyk
    @BeforeEach
    public void createNewBasket(){
        myBasket = new Basket();
    }

    // metoda testowa, testuje metodę returnBasket()
    @Test
    public void testReturnBasket(){
        // tworzymy książkę i dodajemy do koszyka
        myBasket.shoppingList.add(new Book("Title", "author", 10.32));
        // wywołujemy testowaną metodę i zapamietujemy zwrócony obiekt
        ArrayList<Book> actual = myBasket.returnBasket();
        // sprawdzamy czy jest to obiekt typu ArrayList i czy ma jeden element
        assertThat(actual)
                .isInstanceOf(ArrayList.class)
                .hasSize(1);
    }
}
