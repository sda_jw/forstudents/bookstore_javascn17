package test;

import app.Book;
import app.BooksList;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.*;

// klasa testowa, testuje klasę BooksList
public class BooksListTest {
    // posiada pole typu BooksList
    // będziemy przypisywać do niego nowe listy książek i na nich testować
    BooksList bList;

    // metoda cyklu życia, wykonuje się przed każdym testem
    // tworzymy w niej nową listę książek
    @BeforeEach
    public void createList(){
        bList = new BooksList();
    }

    // metoda testowa, testuje metodę addBooks()
    @Test
    public void testAddBook(){
        // tworzymy książkę
        Book book = new Book("Title", "Author", 20.5);
        // zapamiętujemy liczbę książek na liście (pusta, 0)
        int elCountBefore = bList.books.size();
        // dodajemy książkę na listę
        bList.addBook(book);
        // sprawdzamy czy po powyższej operacji lista ma o 1 więcej książek (w tym przypadku 1)
        assertThat(bList.books.size()).isEqualTo(elCountBefore + 1);
    }
}
